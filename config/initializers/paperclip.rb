Paperclip::Attachment.default_options[:url] = 'masayoshi.s3.amazonaws.com' 
# '[バケット名].s3.amazonaws.com'という形にすると良いっぽいです。

Paperclip::Attachment.default_options[:path] = '/:class/:attachment/:id_partition/:style/:filename'

Paperclip::Attachment.default_options[:s3_host_name] = 's3-ap-northeast-1.amazonaws.com'