class Pic < ApplicationRecord
  acts_as_votable
  belongs_to :user
  
  has_attached_file :image, 
                    :styles => { :medium => "300x300>" },   
                    :storage => :s3,
                    :s3_credentials => {
                    :bucket  => ENV['AMAZON_S3_BUCKET_NAME'],
                    :access_key_id => ENV['AMAZON_ACCESS_KEY_ID'],
                    :secret_access_key => ENV['AMAZON_SECRET_ACCESS_KEY'] },
                    :path => ":attachment/:id/:style.:extension"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
